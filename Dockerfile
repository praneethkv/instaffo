FROM rust:1.75-alpine AS build

RUN apk add --no-cache musl-dev

RUN mkdir -p /usr/src/instarust

WORKDIR /usr/src/instarust

COPY ./ /usr/src/instarust

RUN cargo build --release

#---------------------------------
FROM alpine:latest

RUN apk add --no-cache libgcc

WORKDIR /usr/src/instarust

COPY --from=build /usr/src/instarust/target/release/instarust ./instarust

EXPOSE 3000

ENV RUST_LOG=debug

CMD ./instarust
