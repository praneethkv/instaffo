## Solution for the tasks

### Task 1: CI/CD pipeline to build and push images

- New stages have been added the pipeline.
- Build stage builds the Docker Image and pushes the image to Registry of the project. Build stage runs only when there is a merge request created or updated.
- Built image has the tag of merge request.
- When the merge request is updated, pipeline will push the image to the existing tag of the same image.
- Cleanup stage removes the image with the merge request tag when the merge request is approved. 
- Merge request ID is determined by reading the recent commit.
- This stage also runs only when commit is done on main branch.

### Task 2: Optimizing the pipeline

- Dockerfile has been updated to use stage builds to reduce the layers of final image.
- Final image will be lighter to pull and push which will be faster.
- The same has been done and image size reduced from 1.67 GB to 17.6 MB.
- Base image is changes to alpine image to make it lighter.
- When a lot of developers are working on the repository. Multiple pipelines will be in queue.
- To make it efficient, we can follow below steps.
- Initiate multiple runners will run multiple pipelines parallelly.
- Use self hosted runners in a powerfull VM.
- Run the pipeline on a vm which will decrease the time since cache can be used of previous runs for faster build.
- Using of incremental builds will reduce the runtime of pipeline since that rebuilding will be done where parts of the project changes.

### Task 3: Monitoring and Alerting

- Promotheus is a great tool to collect metrics of the application.
- Using of promotheus will help us to collect the metrics and understand the usage of the application.
- Implementation of Fluentd will collect logs from the application and saves them in a central location. Elasticsearch can be used to retrieve the logs.
- Additionally Grafana can be used to visualize if needed.
- Alertmanager can send alert notifications depending on the conditions.

### Task 4: Runtime configuration in Kubernetes

- Different set of templates in helm can be used for production and testing.
- Configmap can be created specifying the different log level and the configmaps can be refered in the pod spec.
- Similarly if different databases are referenced in staging and production, different configmaps, and secrets be referenced indicating to respective environment.